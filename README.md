# Node-RED

http://nodered.org

[![Build Status](https://travis-ci.org/node-red/node-red.svg?branch=master)](https://travis-ci.org/node-red/node-red)
![Coverage Status](https://coveralls.io/repos/node-red/node-red/badge.svg?branch=master)
![NPM](https://img.shields.io/npm/l/node-red-dashboard)

Low-code programming for event-driven applications.

![Node-RED: Low-code programming for event-driven applications](http://nodered.org/images/node-red-screenshot.png)


### Original Repository

This repository is a clone of the official [Node-RED](https://github.com/node-red/node-red) repository. It may contain additional features or modifications that are specific to our use case.

[![Forked from](https://img.shields.io/badge/Forked%20from-Node--RED-blue)](https://github.com/node-red/node-red)

### Keeping the repository up-to-date
You can keep your repository up-to-date with the original by adding the original repository as a remote and pulling the changes.

        git remote add upstream https://github.com/node-red/node-red
        git pull upstream master


## Quick Start

Check out http://nodered.org/docs/getting-started/ for full instructions on getting
started.

1. `sudo npm install -g --unsafe-perm node-red`
2. `node-red`
3. Open <http://localhost:1880>

## Getting Help

More documentation can be found [here](http://nodered.org/docs).

For further help, or general discussion, please use the [Node-RED Forum](https://discourse.nodered.org) or [slack team](https://nodered.org/slack).

## Developers

If you want to run the latest code from git, here's how to get started:

1. Clone the code:

        git clone https://github.com/node-red/node-red.git
        cd node-red

2. Install the node-red dependencies

        npm install

3. Build the code

        npm run build

4. Run

        npm start


## Copyright and license

Copyright OpenJS Foundation and other contributors, https://openjsf.org under [the Apache 2.0 license](LICENSE).
